-- script de atualizacao do BASE com dados importados da ViaCerta - Fabio - 03/2020

set serveroutput on size 100000
set heading off
set pagesize 0
set verify off
set feedback off
set linesize 1000
set termout on

declare 
  v_linha        varchar2(2000);
  v_nomelgra     varchar2(10);
  v_novologra    geral.clientes_viacerta.logradouro1%type;
  v_obs          varchar2(1000);
  v_erro         varchar2(500);
  v_dados         varchar2(1000);
  v_tem_cep       number(5);
  v_commit        varchar2(2);
  v_atual         number(5); 
  v_cpf               geral.clientes_viacerta.cpf%type;
  v_tipologradouro1   geral.clientes_viacerta.tipologradouro1%type;  --base_end
  v_logradouro1       geral.clientes_viacerta.logradouro1%type;      --base_end
  v_numero1           geral.clientes_viacerta.numero1%type;          --bae_endn
  v_bairro1           geral.clientes_viacerta.bairro1%type;          --base_bai
  v_cep1              geral.clientes_viacerta.cep1%type;             --base_cep
  v_id                geral.clientes_viacerta.id%type;
  v_datanascimento    geral.clientes_viacerta.datanascimento%type;   --base_nsc
  v_telefone1         geral.clientes_viacerta.telefone1%type;        --base_cel
  v_celular1          geral.clientes_viacerta.celular1%type;         --base_fon
  v_status_cpf_rf     geral.clientes_viacerta.status_cpf_rf%type;
  v_complemento       geral.clientes_viacerta.complemento%type;      --base_comple
  v_whatsapp          geral.clientes_viacerta.whatsapp%type;         --base_whats
  v_nomeempregador    geral.clientes_viacerta.nomeempregador%type;   --base_user2
  v_email1            geral.clientes_viacerta.email1%type;           --base_eml
  v_telefone1mae      geral.clientes_viacerta.telefone1mae%type;
  v_celular1mae       geral.clientes_viacerta.celular1mae%type;
  v_telefone1irmao1   geral.clientes_viacerta.telefone1irmao1%type;
  v_celular1irmao1    geral.clientes_viacerta.celular1irmao1%type;
  v_telefone1filho1   geral.clientes_viacerta.telefone1filho1%type;
  v_celular1filho1    geral.clientes_viacerta.celular1filho1%type;
  v_provavelobito     geral.clientes_viacerta.provavelobito%type;
 
  v_uf                varchar2(2);
  v_cidade            varchar2(50);

  v_base_end    base.base_end%type; 
  v_base_endn    base.base_endn%type;
  v_base_bai    base.base_bai%type; 
  v_base_uf    base.base_uf%type;  
  v_base_cep    base.base_cep%type; 
  v_base_eml    base.base_eml%type; 
  v_base_fon    base.base_fon%type; 
  v_base_cel    base.base_cel%type; 
  v_base_dac    base.base_dac%type; 

  v_para_fon    base.base_fon%type; 
  v_para_cel    base.base_cel%type; 

  v_base_comple  base.base_comple%type;
  v_base_whats   base.base_whats%type;
  v_base_user2   base.base_user2%type;
  v_base_sta1    base.base_sta1%type;
  v_base_sta     base.base_sta%type;
  
  v_cid_codigo   geral.cep_cidade.cep%type; 
  v_cid_cidade   geral.cep_cidade.cidade%type;
  v_cid_uf       geral.cep_cidade.uf%type;
  v_cid_codmun   geral.cep_cidade.cod_ibge%type;
  v_cid_cidsub   geral.cep_cidade.id_municipio_subordinado%type;
  
-- o cursor somente pega o que for status = 1 importados
  cursor item is
    select ID, CPF, trim(TIPOLOGRADOURO1),LOGRADOURO1,NUMERO1,substr(BAIRRO1,1,20),
         CEP1, TELEFONE1,CELULAR1,EMAIL1, COMPLEMENTO, whatsapp, 
         upper(provavelobito), upper(status_cpf_rf),upper(uf1), telefone1mae, 
         celular1mae, telefone1irmao1, celular1irmao1, telefone1filho1, celular1filho1
      from geral.clientes_viacerta
      where status=1
--and cpf IN (
--73948209049,
--35038535020
--)   
order by id
    ;

begin
    v_atual:=0;
    select count(*) into v_atual
      from geral.clientes_viacerta
      where status=1;

   if v_atual > 0 then 
    dbms_output.put_line(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')||'==>>> Inicio do processo. Total: '||to_char(v_atual,'99990'));
  if v_atual>0 then 


    v_atual:=0;
   
    Open item;
    loop
        Fetch item Into v_id, v_cpf, v_tipologradouro1,v_logradouro1, v_numero1,v_bairro1,v_cep1,
                        v_telefone1,v_celular1,v_email1, v_complemento, v_whatsapp,
                        v_provavelobito, v_status_cpf_rf, v_uf, v_telefone1mae, v_celular1mae, 
                        v_telefone1irmao1, v_celular1irmao1, v_telefone1filho1, v_celular1filho1;
        Exit When item%NotFound;
        v_commit:='ok';
        v_obs:='';
        v_dados:='';
           
--busca os campos que podem ser atualizado 
        select base_end, base_endn, base_bai, base_uf, base_cep, 
               base_eml, base_fon, base_cel, base_comple, base_whats, 
               base_user2, base_sta1, base_sta, base_dac
            into v_base_end, v_base_endn, v_base_bai, v_base_uf, v_base_cep, 
                 v_base_eml, v_base_fon, v_base_cel, v_base_comple, v_base_whats, 
                 v_base_user2, v_base_sta1, v_base_sta, v_base_dac  
            from base
                       where base_cpf=v_cpf;
--registra como estava antes 
        v_dados:='Estava assim: baseend='||v_base_end||' - baseendn='||v_base_endn||' - ';
        v_dados:=v_dados||'basebai='||v_base_bai||' - baseuf='||v_base_uf||' - ';
        v_dados:=v_dados||'basecep='||v_base_cep||' - baseeml='||v_base_eml||' - ';
        v_dados:=v_dados||'basefon='||v_base_fon||' - basecel='||v_base_cel;
        v_dados:=v_dados||'basesta='||v_base_sta||' - basesta1='||v_base_sta1;

--atualização cadastral maior que 180 dias
        if (sysdate-v_base_dac)>=180 then   

-- define o nome do tipo de logradouro 
            v_nomelgra:='';
            if v_tipologradouro1 is not null then 
                begin 
                    select upper(trim(expressao)) into v_nomelgra
                        from geral.abreviacerta 
                        where abreviatura=v_tipologradouro1;
                exception 
                    when no_data_found then 
                        null;
                end;
            end if;

-- junta o tipo de logradouro com o logradouro 
            if v_nomelgra is null then 
                if v_logradouro1 is null then
                    v_novologra:=v_base_end;
                else
                    v_novologra:=v_logradouro1;
                end if;
                v_obs:='#Tp Logra nao definido='||v_tipologradouro1||'  ';
            else 
                v_novologra:=v_nomelgra||' '||v_logradouro1;        
            end if;

            begin   
-- verifica se o cep ta cadastrado na nova tabela de ceps
                select upper(cid.cidade), upper(cid.uf) into v_cidade, v_uf
                    from geral.cep_cidade cid 
                    where cid.cep=v_cep1;
                v_tem_cep:=0;
                begin
                    select count(*) into v_tem_cep
                        from geral.cep
                        where codigo=v_cep1;

                    if v_tem_cep=0 then
                        v_cid_codigo:='';
                        v_cid_cidade:='';
                        v_cid_uf:='';
                        v_cid_codmun:='';
                        v_cid_cidsub:='';
                           
                        select cend.cep, upper(cid.cidade), upper(cid.uf),  
                               decode(nvl(cid.COD_IBGE,'0'),'0',(select ccd.COD_IBGE from geral.cep_cidade ccd
                                                                    where ccd.id_cidade = cid.id_municipio_subordinado),cid.COD_IBGE),
                               nvl(upper(decode(nvl(cid.COD_IBGE,'0'),'0',(select ccd.cidade from geral.cep_cidade ccd
                                                                              where ccd.id_cidade = cid.id_municipio_subordinado))),'0')
                            into v_cid_codigo, v_cid_cidade, v_cid_uf, v_cid_codmun, v_cid_cidsub
                            from geral.cep_endereco cend, geral.cep_cidade cid
                            where cend.cep= v_cep1
                            and cid.ID_CIDADE=cend.ID_CIDADE;
                            
                        begin 
                            insert into geral.cep (codigo, cidade, uf, bairro, lograd, codmun)
                                values (v_cep1, v_cid_cidade, v_cid_uf, v_bairro1, v_novologra, v_cid_codmun);

                        exception 
                            when others then 
                                v_obs:=v_obs||'#CEP nao incluido no GERAL.CEP='||v_cep1||'  ';
                                v_commit:='no';
                        end;
                
                    end if;
                end;
            exception  
                when no_data_found then 
                    v_obs:=v_obs||'#CEP nao tem no GERAL.CEP_CIDADE='||v_cep1||'  ';      
                    v_tem_cep:=0;
                    select count(*) into v_tem_cep
                        from geral.cep
                        where codigo=v_cep1;

                    if v_tem_cep=0 then
                        v_obs:=v_obs||'#CEP nao incluido no GERAL.CEP='||v_cep1||'  ';
                        v_commit:='no'; 
                    end if;
            end;

--base_fon  CELULAR1  deve ter apenas celular
--base_cel  TELEFONE1
            v_para_cel:='';
            v_para_fon:='';
            v_para_cel:=v_telefone1;
                   
            if v_celular1 is not null then 
                if substr(v_celular1,3,1)=9 then 
                    if v_para_fon is null then
                        v_para_fon:=v_celular1;
                    else 
                        v_para_cel:=v_para_fon;
                        v_para_fon:=v_celular1;
                    end if;
                end if;
            end if;

            if v_commit<>'no' then
--testa status 
                if v_base_sta=4 then
                    if (v_status_cpf_rf<>'REGULAR') and (v_status_cpf_rf<>'TITULAR FALECIDO') then 
                        v_base_sta:=2;
                    end if;
                end if;
--atualiza os dados no base 
                update base 
                    set base_end=decode(v_novologra,v_base_end,v_base_end,v_novologra),
                        base_endn=decode(v_numero1,v_base_endn,v_base_endn,v_numero1),
                        base_comple=decode(v_complemento,v_base_comple,v_base_comple,v_complemento),
                        base_whats=decode(v_whatsapp,'SIM','S','N'),
                        base_bai=decode(v_bairro1,v_base_bai,v_base_bai,decode(v_bairro1,null,v_base_bai,v_bairro1)),
                        base_uf=decode(v_uf,v_base_uf,v_base_uf,v_uf),
                        base_cep=decode(v_cep1,0,v_base_cep,v_cep1),
                        base_eml=v_email1,
                        base_fon=decode(v_base_fon,v_para_fon,v_base_fon,decode(v_para_fon,null,v_base_fon,v_para_fon)),
                        base_cel=decode(v_base_cel,v_para_cel,v_base_cel,decode(v_para_cel,null,v_base_cel,v_para_cel)),
                        base_sta1=decode(v_provavelobito,'SIM','F',base_sta1),
                        base_sta=v_base_sta
                    where base_cpf=v_cpf;
-- se mofificou, atualiza a data de alteracao cadastral              
                if sql%rowcount > 0 then 
                    update base 
                        set base_dac=sysdate
                        where base_cpf=v_cpf;
-- caso tenha telefones de mae, inclui na tabela base_fones
                    if (v_telefone1mae||v_celular1mae) is not null then
                        begin
                            if length(v_telefone1mae)>=10 then 
                                if length(v_celular1mae)=11 and substr(v_celular1mae,3,1)=9 then 
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                        values 
                                           (v_cpf,'2',v_telefone1mae, v_celular1mae);
                                else
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                        values 
                                          (v_cpf,'2',v_telefone1mae, null);
                                end if;      
                            else 
                                if length(v_celular1mae)=11 and substr(v_celular1mae,3,1)=9 then 
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                        values 
                                          (v_cpf,'2',null, v_celular1mae);
                                end if;
                            end if;
                    
                        exception 
                            when dup_val_on_index then 
                                if length(v_telefone1mae)>=10 then 
                                    if length(v_celular1mae)=11 and substr(v_celular1mae,3,1)=9 then 
                                        update CADASTRO.BASE_FONES
                                            set num_fone=v_telefone1mae, num_cel=v_celular1mae
                                            where cpf_titular=v_cpf;
                                    else
                                        update CADASTRO.BASE_FONES
                                            set num_fone=v_telefone1mae, num_cel=null
                                            where cpf_titular=v_cpf;
                                    end if;      
                                else 
                                    if length(v_celular1mae)=11 and substr(v_celular1mae,3,1)=9 then 
                                        update CADASTRO.BASE_FONES
                                            set num_cel=v_celular1mae, num_fone=null
                                            where cpf_titular=v_cpf;
                                    end if;
                                end if;
                        end;
                        commit;
                    end if;
-- caso tenha telefones de irmao, inclui na tabela base_fones
                    if (v_telefone1irmao1||v_celular1irmao1) is not null then
                        begin
                            if length(v_telefone1irmao1)>=10 then 
                                if length(v_celular1irmao1)=11 and substr(v_celular1irmao1,3,1)=9 then 
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                        values 
                                           (v_cpf,'6',v_telefone1irmao1, v_celular1irmao1);
                                else
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                        values 
                                          (v_cpf,'6',v_telefone1irmao1, null);
                                end if;      
                            else 
                                if length(v_celular1irmao1)=11 and substr(v_celular1irmao1,3,1)=9 then 
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                        values 
                                          (v_cpf,'6',null, v_celular1irmao1);
                                end if;
                            end if;
                    
                        exception 
                            when dup_val_on_index then 
                                if length(v_telefone1irmao1)>=10 then 
                                    if length(v_celular1irmao1)=11 and substr(v_celular1irmao1,3,1)=9 then 
                                        update CADASTRO.BASE_FONES
                                            set num_fone=v_telefone1irmao1, num_cel=v_celular1irmao1
                                            where cpf_titular=v_cpf;
                                    else
                                        update CADASTRO.BASE_FONES
                                            set num_fone=v_telefone1irmao1, num_cel=null
                                            where cpf_titular=v_cpf;
                                    end if;      
                                else 
                                    if length(v_celular1irmao1)=11 and substr(v_celular1irmao1,3,1)=9 then 
                                        update CADASTRO.BASE_FONES
                                            set num_cel=v_celular1irmao1, num_fone=null
                                            where cpf_titular=v_cpf;
                                    end if;
                                end if;
                        end;
                        commit;
                    end if;      
                       
-- caso tenha telefones de filho, inclui na tabela base_fones
                    if (v_telefone1filho1||v_celular1filho1) is not null then 
                        begin
                            if length(v_telefone1filho1)>=10 then 
                                if length(v_celular1filho1)=11 and substr(v_celular1filho1,3,1)=9 then 
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                       values 
                                          (v_cpf,'8',v_telefone1filho1, v_celular1filho1);
                                else
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                        values 
                                          (v_cpf,'8',v_telefone1filho1, null);
                                end if;      
                            else 
                                if length(v_celular1filho1)=11 and substr(v_celular1filho1,3,1)=9 then 
                                    insert into CADASTRO.BASE_FONES (CPF_TITULAR, ID_RELACIONAMENTO, NUM_FONE, NUM_CEL)
                                       values 
                                         (v_cpf,'8',null, v_celular1filho1);
                                end if;
                            end if;
                        exception 
                            when dup_val_on_index then 
                                if length(v_telefone1filho1)>=10 then 
                                    if length(v_celular1filho1)=11 and substr(v_celular1filho1,3,1)=9 then 
                                        update CADASTRO.BASE_FONES
                                            set num_fone=v_telefone1filho1, num_cel=v_celular1filho1
                                            where cpf_titular=v_cpf;
                                    else
                                        update CADASTRO.BASE_FONES
                                            set num_fone=v_telefone1filho1, num_cel=null
                                            where cpf_titular=v_cpf;
                                    end if;      
                            else 
                                if length(v_celular1filho1)=11 and substr(v_celular1filho1,3,1)=9 then 
                                    update CADASTRO.BASE_FONES
                                        set num_cel=v_celular1filho1, num_fone=null
                                        where cpf_titular=v_cpf;
                                end if;
                            end if;
                        end;
                        commit;
                    end if;

                    begin
-- registra na tabela a atulizacao do cpf registrando os dados que tinha antes de aleterar                  
                        insert into CADASTRO.AUDITORIA 
                            (SISTEMA_LEGADO, CHAVE_LEGADO, DADOS, OBSERVACAO, STATUS_ID)
                               values 
                                 ('ATUALIZA_CLIENTES_VIACERTA', 'CPF: '||to_char(v_cpf,'00000000000')||' ATUALIZADO', 
                                  utl_raw.cast_to_raw(v_dados),v_obs, 0);
-- muda status para 2 = atualizado 
                        update geral.clientes_viacerta
                            set status=2, observacao=v_obs
                            where id=v_id;
                        v_atual:=v_atual+1;
                        commit; 
                    exception 
                        when others then 
                            v_commit:='no';
                            rollback;
                            dbms_output.put_line('CPF: '||to_char(v_cpf,'00000000000')||' >> ERRO: '||SQLERRM);
                    end;
                    v_obs:='';
                else
                    v_obs:=v_obs||'#Nenhuma Alteracao efetuada';
-- muda status para 3 = nada feito 
                    rollback;
                    update geral.clientes_viacerta
                        set status=3, observacao=v_obs
                        where id=v_id;
                    commit;
                end if;
            else 
               update geral.clientes_viacerta
                  set status=3, observacao=v_obs
                  where id=v_id;
                commit;            
            end if;  -- if v_commit=no 
-- se tiver algum dado de observacao ou nao atualizou o registro do cpf, registra na auditoria                    
            if v_obs is not null then 
                begin
                    insert into CADASTRO.AUDITORIA 
                        (SISTEMA_LEGADO, CHAVE_LEGADO, DADOS, OBSERVACAO, STATUS_ID)
                            values 
                              ('ATUALIZA_CLIENTES_VIACERTA', 'CPF: '||to_char(v_cpf,'00000000000')||' NADA ATUALIZADO', 
                               utl_raw.cast_to_raw(v_obs), 'PROBLEMAS DURANTE UPDATE BASE', 1);
                    commit;
                exception 
                    when others then 
                        rollback;
                        dbms_output.put_line('CPF: '||to_char(v_cpf,'00000000000')||' >> ERRO: '||SQLERRM);
                                
                end;
            end if;
        else
-- muda status para 4 = Cadastro alterado recentemente  
            update geral.clientes_viacerta
                set status=4, observacao='Ult.Alteracao: '||to_char(v_base_dac,'dd/mm/rrrr')
                where id=v_id;
            commit;
        end if;

    end loop;
    close item;
--    dbms_output.put_line('resultado: '||v_commit||'    obs:'||v_obs);
    v_tem_cep:=0;
    select count(*) into v_tem_cep
      from geral.clientes_viacerta
      where status=2;

    dbms_output.put_line('Atualizados: '||to_char(v_tem_cep,'99990'));

	v_obs:='';
    v_tem_cep:=0;
    select count(*) into v_tem_cep
      from geral.clientes_viacerta
      where status=1;
      
    dbms_output.put_line('Pendentes: '||to_char(v_tem_cep,'999990'));
  end if;
  dbms_output.put_line(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')||'==>>> Fim do processo..');
 end if;
end;
/
exit

