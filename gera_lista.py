import cx_Oracle
import psycopg2 as pg
import pandas as pd
import threading
import numpy as np
from datetime import datetime, date, timedelta

def buscaCPFsiscobra():    
    # cria conexao com o Siscobra 
    conSiscobra = pg.connect("host=172.16.3.248 port=5432 dbname=siscobraweb user=siscobraweb password=siscobraweb")
    sql = """
          SELECT d.devcpf
              FROM devedor d, pessoa p, telefone t 
              WHERE d.devcod = p.devcod 
              AND p.pescod = t.pescod 
              AND t.telvalpon <= -20 
              AND d.devati = 0
              AND d.devsal > 0
    """
    df = pd.DataFrame()
    df = pd.read_sql(sql,conSiscobra)
    conSiscobra.close()  
    return df

# cria a conexao com o Oracle 
conOracle = cx_Oracle.connect('cadastro/informa@172.16.2.100/orcl', encoding = "UTF-8", nencoding = "UTF-8") 

# monta consulta na tabela BASE 
sqlbase = " select base_cpf, base_nom, base_sex, base_nsc, base_nma, base_end, substr(base_endn,1,6), base_bai,"
sqlbase += " base_uf, base_cep, replace(base_cel, ' ',''), replace(base_fon, ' ',''), base_eml, 0, sysdate, null"
sqlbase += " from base"
# monta o insert dos dados em GERAL.CLIENTES_VIACERTA
sqlins = "insert into GERAL.CLIENTES_VIACERTA (CPF, NOME, SEXO, DATANASCIMENTO, NOMEMAE, LOGRADOURO1, NUMERO1, BAIRRO1, "
sqlins += " UF1, CEP1, TELEFONE1, CELULAR1, EMAIL1, STATUS, DT_IMPORTACAO, OBSERVACAO)"
# acrescenta filtro na consulta para pegar apenas clientes novos 
sql = sqlins + sqlbase
sql += " where base_dtc>(sysdate-1)"
# executa o procedimento 
cur = conOracle.cursor()
cur.execute(sql)
conOracle.commit()
print(cur.rowcount,"Clientes novos incluidos ")
# cur.close()

# monta a lista de cpfs extraida do Siscobra 
cpfs = buscaCPFsiscobra() 
numregis=0
# para cada cpf, busca no BASE os dados a serem incluidos no GERAL.CLIENTES_VIACERTA
for cpf in cpfs.itertuples():
    # print(cpf.devcpf)
    # monta o sql incluido a consulta e o insert na tabela, do cpf extraida da consulta siscobra 
    sql = sqlins + sqlbase
    sql += " where base_cpf=%d" % (cpf.devcpf)
    # print(sql)
   #  cur = conOracle.cursor()
    cur.execute(sql)
    conOracle.commit()
    numregis = numregis + 1
print(str(numregis),"Clientes do Siscobra")
cur.close()
conOracle.close()

# cria a conexao com o Oracle usuario GERAL 
conOracle = cx_Oracle.connect('geral/nenhuma@172.16.2.100/orcl', encoding = "UTF-8", nencoding = "UTF-8") 
cur = conOracle.cursor()

# monta consulta para gerar arquivo csv
sql = """
         select ID, CPF from GERAL.CLIENTES_VIACERTA where status=0
      """
# executa consulta criando uma lista 
df = pd.DataFrame()
df = pd.read_sql(sql,conOracle)

# loop para armazenar o resultado 
paraarquivo = ''
for registro in df.itertuples():
# pega a primeira parte da consulta 
    paraarquivo = paraarquivo + str(registro.CPF) + ";\n"
# marca o registro como listado para envio a ViaCerta
    idcpf = registro.ID
    datalis = date.today()
    datalis = datalis.strftime('%d/%m/%Y')
    obs = "Lista gerada em %s" % (datalis)
    sqlupd = "update GERAL.CLIENTES_VIACERTA set status=5, observacao='%s' where status=0 and ID=%d" % (obs,idcpf)
    cur.execute(sqlupd)
    conOracle.commit()
cur.close()
conOracle.close()

# nome do arquivo para envio CLIENTES_ENVIO_aaaa-mm-dd.CSV
file = "CLIENTES_ENVIO_"
extension = "csv"

data = str((date.today() - timedelta(1)))
data = data[:10]

#filename = "T:\Scripts\cvp\ViaCerta\Sai_Monjua_3390\\" + file + data + "." + extension
filename = "/home/viacliente/" + file + data + "." + extension

# Escreve o resultado em arquivo formato csv
with open(filename,"a") as arquivo:
    arquivo.write(paraarquivo)
    