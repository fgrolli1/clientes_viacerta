import cx_Oracle
import json

class Auditoria():
    def __init__(self):
        self.conexao = None
        self.cursor = None
        self.teste = None
        

    def setTeste(self, teste):
        self.teste = teste

    def conectarBD(self):
        if self.teste == True:
            stringConexao = 'CONSULTOR/senha@172.16.5.161:1521/orcl'
        else:
            stringConexao = 'CONSULTOR/senha@172.16.2.100:1521/orcl'
        self.conexao = cx_Oracle.connect(stringConexao, encoding = "UTF-8", nencoding = "UTF-8")
        self.cursor = self.conexao.cursor()

    def desconectarBD(self):
        self.cursor.close()
        self.conexao.rollback()
        self.conexao.close()

    def gravar(self, sistema, chave, dados, observacao, status):
        sql = """insert into CADASTRO.AUDITORIA 
                             (SISTEMA_LEGADO, CHAVE_LEGADO, DADOS, OBSERVACAO, STATUS_ID)
                      values (:SISTEMA_LEGADO, :CHAVE_LEGADO, utl_raw.cast_to_raw(:DADOS), :OBSERVACAO, :STATUS_ID)"""
        
        dados = json.dumps(dados, ensure_ascii = False, default = str, separators = (',', ':'))
        self.cursor.execute(sql,
            {
                'SISTEMA_LEGADO' :sistema,
                'CHAVE_LEGADO' :chave,
                'DADOS' :dados,
                'OBSERVACAO' :observacao,
                'STATUS_ID' : status
            })
        self.conexao.commit()
