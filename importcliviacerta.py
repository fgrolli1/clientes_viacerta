# importação de clientes higienizados pela ViaCerta - Fabio - 06/2020


import ftplib
import os
from datetime import datetime, date, timedelta
import csv
import cx_Oracle
from auditoria import Auditoria

enderecoIP = "192.168.0.32"
login = "viacliente"
password = "vi@c34t@cl!3nt3"

ftp = ftplib.FTP(enderecoIP)
ftp.login(login, password)

pathFTP = "/home/viacliente/"
pathLocal = "/home/pagina/pythonApps/importacao_viacerta/clientes_viacerta/recebidos/"
file = "CLIENTES_RETORNO_"
extension = "csv"

#data = str((date.today() - timedelta(1)))
data = str((date.today()))
data = data[:10]

ftp.cwd(pathFTP)
filename = file + data + "." + extension

dataHora = str(datetime.now()).replace("-", "")
dataHora = dataHora.replace(" ", "_")
dataHora = dataHora.replace(":", "")
dataHora = dataHora[:15]

auditoria = Auditoria()
auditoria.setTeste(False)
auditoria.conectarBD()
sistema = "IMPORT_CLIENTES_VIACERTA"
chave = filename

observacao = "INÍCIO DO PROCESSO: " + str(datetime.now())
dados = {"PROCESSO": "INICIADO"}
auditoria.gravar(sistema, chave, dados, observacao, 0)

try:
    dadosConexao = 'geral/nenhuma@172.16.2.100:1521/orcl'
    conexao = cx_Oracle.connect(dadosConexao, encoding = "UTF-8", nencoding = "UTF-8")
    cursor = conexao.cursor()

    ftp.retrbinary("RETR " + filename, open(filename, "wb").write)

    observacao = "DOWNLOAD CONCLUÍDO AS " + str(datetime.now())
    dados = {"DOWNLOAD": "CONCLUÍDO COM SUCESSO"}
    auditoria.gravar(sistema, chave, dados, observacao, 0)

    with open(filename, "r", encoding='ISO-8859-1') as ficheiro:
#    with open(filename, "r", encoding='utf-8-sig') as ficheiro:
        reader = csv.reader(ficheiro, delimiter=';', quoting=csv.QUOTE_NONE)

        for linha in reader:

            sql = """INSERT INTO CLIENTES_VIACERTA (CPF, STATUS_CPF_RF, NOME, SEXO, DATANASCIMENTO, 
                        NOMEMAE, TIPOLOGRADOURO1, LOGRADOURO1, NUMERO1, COMPLEMENTO, BAIRRO1, UF1, 
                        CEP1, CELULAR1, WHATSAPP, TELEFONE1, EMAIL1, EMPREGADOR, NOMEEMPREGADOR,  
                        TELEFONE1MAE, CELULAR1MAE, TELEFONE1IRMAO1, CELULAR1IRMAO1, 
                        TELEFONE1FILHO1, CELULAR1FILHO1, PROVAVELOBITO, STATUS)
                            VALUES('""" + linha[0] + """',
                                   '""" + linha[1] + """',
                                   '""" + linha[2].replace("'"," ") + """',
                                   '""" + linha[3] + """',
                                   to_date('""" + linha[4] + """', 'dd/mm/yyyy'),
                                   '""" + linha[9].replace("'"," ") + """',
                                   '""" + linha[10] + """',
                                   '""" + linha[11] + """',
                                   '""" + linha[12] + """',
                                   '""" + linha[13] + """',
                                   '""" + linha[14] + """',
                                   '""" + linha[16] + """',
                                   '""" + linha[17] + """',
                                   '""" + linha[18] + """',
                                   '""" + linha[19] + """',
                                   '""" + linha[26] + """',
                                   '""" + linha[30] + """',
                                   '""" + linha[31] + """',
                                   '""" + linha[32].replace("'"," ") + """',
                                   '""" + linha[41] + """',
                                   '""" + linha[43] + """',
                                   '""" + linha[45] + """',
                                   '""" + linha[47] + """',
                                   '""" + linha[49] + """',
                                   '""" + linha[51] + """',
                                   '""" + linha[53] + """',
                                   '""" + '1' + """')"""
            try:
                observacao = "INSERÇÃO DE LINHA"
                dados = {"COMANDO_INCLUSÃO": sql}
                cursor.execute(sql)
                auditoria.gravar(sistema, chave, dados, observacao, 0)
                conexao.commit()
            except Exception as e:
                conexao.rollback()
                observacao = "ERRO >> " + str(e)
                dados = {"COMANDO_INCLUSÃO": sql, "EXCEPTION": str(e)}
                auditoria.gravar(sistema, chave, dados, observacao, 1)
    filename_new = pathLocal + filename
    os.link(filename, filename_new)
    os.remove(filename)
    ftp.delete(filename)
    ftp.quit()
except Exception as e:
    conexao.rollback()
    observacao = "ERRO >> " + str(e)
    dados = {"EXCEPTION": str(e)}
    auditoria.gravar(sistema, chave, dados, observacao, 1)
    print("Falha ao importar o arquivo >> " + filename + ": " + str(e))
    pathLocal = "/home/pagina/"
    os.remove(pathLocal + filename)    
finally:
    observacao = "TÉRMINO DO PROCESSO: " + str(datetime.now())
    dados = {"PROCESSO": "ENCERRADO"}
    auditoria.gravar(sistema, chave, dados, observacao, 3)
    auditoria.desconectarBD()
    cursor.close()
    conexao.close()
	
	
	